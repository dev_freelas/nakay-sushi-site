<!-- 
    > name
    > columns
    > data
-->
<div class="container">
    <table id="tb-{{ $name }}" class="table table-striped table-bordered">
        <thead class="thead-light">
            <tr>
                @foreach( $columnNames as $column )
                    <th scope="col">{{ ucfirst($column) }}</th>
                @endforeach
            </tr>
        </thead>
    </table>
</div>


@section('script')
<script type="text/javascript">
    var cols = [];
    @foreach( $columns as $column )
        @if (count($column) == 1)
            cols.push( { data: '{{ $column[0] }}', name: '{{  $column[0]  }}' } );
        @else
            @switch($column[1])
                @case('bool')
                    cols.push( { data: '{{ $column[0] }}', name: '{{  $column[0]  }}', 
                        render: function ( data, type, row ) {
                            return (data == '1') ? 'Sim' : 'Não';
                        } } );
                    @break

                @case('$')
                    cols.push( { data: '{{ $column[0] }}', name: '{{  $column[0]  }}', 
                            render: function ( data, type, row ) {
                                return 'R$' + data;
                            } } );
                @break

                @case('pc')
                    cols.push( { data: '{{ $column[0] }}', name: '{{  $column[0]  }}', 
                            render: function ( data, type, row ) {
                                return (data > 1) ? data + ' pcs' : data + ' pc';
                            } } );
                @break

                @case('edit')
                    cols.push( { data: '{{ $column[0] }}', name: '{{  $column[0]  }}', 
                        orderable: false, searchable: false});
                @break

            @endswitch    
        @endif     
    @endforeach
    
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#tb-{{ $name }}').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 1, "desc" ]],
            ajax: '{!! url( '/products/ajaxDataTable' ) !!}',
            columns: cols
        });
    });
</script>
@endsection