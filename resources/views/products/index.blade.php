@extends('layouts.app')

@section('style')
<style>
    table.dataTable thead .sorting:after,
    table.dataTable thead .sorting:before,
    table.dataTable thead .sorting_asc:after,
    table.dataTable thead .sorting_asc:before,
    table.dataTable thead .sorting_asc_disabled:after,
    table.dataTable thead .sorting_asc_disabled:before,
    table.dataTable thead .sorting_desc:after,
    table.dataTable thead .sorting_desc:before,
    table.dataTable thead .sorting_desc_disabled:after,
    table.dataTable thead .sorting_desc_disabled:before {
        bottom: .5em;
    }
    #bt-add {
        float: right;
        right: 25px;
        position: absolute;
    }

    .card-header {
        display: inline-flex;
        position: relative;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h1>Lista de produtos</h1>
                    <a id="bt-add" href="{{ route('products.create') }}"><button type="button" class="btn btn-success">Add</button></a></div>
                <div class="card-body">
                    @component('layouts/table', [
                        'name' => 'products', 
                        'columnNames' => ['#', 'Nome', 'Preço', 'Quant.', 'Status', '' ],
                        'columns' => [['id'], ['name'], ['price', '$'], ['count', 'pc'], ['status', 'bool'], ['action', 'edit'] ], 
                        'data' => []
                    ])                        
                    @endcomponent

                </div>
            </div>
        </div>
    </div>
</div>
@endsection