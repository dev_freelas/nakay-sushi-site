<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(3),
        'price' => $faker->randomFloat(2, 0, 100),
        'description' => $faker->paragraph,
        'count' => $faker->numberBetween(1, 6),
        'status'=> true,
        'image' => 'no-image'
    ];
});
