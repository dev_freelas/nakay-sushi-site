# Nakay Sushi

![Codeship Status for Projetos_para_terceiros/nakay-sushi-site](https://app.codeship.com/projects/f422a100-166f-0137-368c-02228243811b/status?branch=master)

**Progresso:** EM ANDAMENTO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2018/2019<br />

### Objetivo
Implementar um sistema de controle de vendas para um restaurante delivery de sushi. O sistema deve conter requisições para um aplicativo do restaurante.

### Observação

IDE:  [Visual Studio Code](https://code.visualstudio.com/)<br />
Linguagem: [PHP 7.3](https://php.net/)<br />
Banco de dados: [MySQL](https://www.mysql.com/)<br />

### Execução

    $ php artisan migrate:fresh --seed
    $ php artisan serve --host=0.0.0.0 --port=8000
    
### Pacotes

| Nome | Função |
| ------ | ------ |
| [Laravel] | Framework base |

[Laravel]: <https://laravel.com/>


### Contribuição

Esse projeto não está aberto a contribuições no momento.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->