<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [ 'name', 'street', 'neighborhood', 'number', 'cep', 'phone', 'city_id' ];
    protected $guardade = [ 'id', 'created_at', 'updated_at' ];
}
