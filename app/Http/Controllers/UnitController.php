<?php

namespace App\Http\Controllers;

use App\Unit;
use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View( 'units.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View( 'units.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'city_id' => 'required',
            'street' => 'required',
            'neighborhood' => 'required',
            'number' => 'required|integer',
            'cep' => 'required|integer'
        ]);

        $unit = new Unit([
            'name' => $request->get('name'),
            'city_id' => $request->get('city_id'),
            'street' => $request->get('street'),
            'neighborhood' => $request->get('neighborhood'),
            'number' => $request->get('number'),
            'cep' => $request->get('cep'),
            'phone' => $request->get('phone')
        ]);

        $unit->save();
        return redirect('/units')->with('success', 'Unit has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        return View('units.show')->with('unit', $unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        return View('units.edit')->with('unit', $unit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        $unit->name = $request->get('name');
        $unit->street = $request->get('street');
        $unit->neighborhood = $request->get('neighborhood');
        $unit->number = $request->get('number');
        $unit->cep = $request->get('cep');
        $unit->phone = $request->get('phone');
        $unit->city_id = $request->get('city_id');
        $unit->save();

        return redirect('/units')->with('success', 'Unit has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        $unit->delete();

        return redirect('/units')->with('success', 'Unit has been deleted');
    }
}
