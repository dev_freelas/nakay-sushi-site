<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if( $request->user() )
            foreach ($roles as $role) {
                if ( $request->user()->role == $role ) {
                    // If user have the role
                    return $next($request);
                }
            }
        else
            // If user is not loged
            return redirect()->back();
        
        // If user not have the role
        return redirect()->route('home');
    }
}
